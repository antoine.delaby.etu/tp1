const data = [
	{
		name: 'Regina',
		base: 'tomate',
		price_small: 6.5,
		price_large: 9.95,
		image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
	},
	{
		name: 'Napolitaine',
		base: 'tomate',
		price_small: 6.5,
		price_large: 8.95,
		image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
	},
	{
		name: 'Spicy',
		base: 'crème',
		price_small: 5.5,
		price_large: 8,
		image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300',
	}
];
let html = '';
// data.sort(function compare(a, b) {
//     if (a.name < b.name)
//         return -1;
//     if (a.name > b.name)
//         return 1;
//     return 0;
// });

// data.sort(function compare(a, b) {
//     if (a.price_small < b.price_small)
//         return -1;
//     if (a.price_small > b.price_small)
//         return 1;
//     return 0;
// });

// data.sort(function compare(a, b) {
//     if (a.price_small < b.price_small)
//         return -1;
//     if (a.price_small > b.price_small)
//         return 1;
//     if (a.price_small == b.price_small){
//         if(a.price_large < b.price_large)
//             return -1;
//         if(a.price_large > b.price_large)
//             return 1;
//     }
//     return 0;
// });

// const result = data.filter(data => data.base == 'tomate');
const result = data.filter(data => data.price_small < 6);

for(let i = 0; i < result.length; i++){
    html += '<article class="pizzaThumbnail"><a href="' + result[i].image + '"><img src="' + result[i].image + '"/><section><h4>' + result[i].name +'</h4><ul><li>Prix petit format : ' + data[i].price_small + '€</li><li>Prix grand format : ' + data[i].price_large +'€</li></ul></section></a></article>';
}
document.querySelector('.pageContent').innerHTML = html;